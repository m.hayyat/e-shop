class AdminsController < ApplicationController
    before_action :confirm_role
    def index
        @users = User.all
    end
    def show
        @user = User.find(params[:id])
    end
    def edit
        @user = User.find(params[:id])
    end
    def update
        @user = User.find(params[:id])
        if @user.update(user_params)
            redirect_to admins_path, notice: "User successfly updated."
        else
            render :edit
        end
    end
    def delete
        @user = User.find(params[:id])
    end

    def destroy
        user = User.find(params[:id]).destroy
        flash[:notice] = "User '#{user.username}' destroyed successfully."
        redirect_to(:action => 'index')
    end

    private
    def user_params
        params.require(:user).permit({role_ids: []})
    end

    def confirm_role
        unless current_user.has_role? :Admin
          redirect_to root_path, alert: "You are not authorized."
        end
    end
end
