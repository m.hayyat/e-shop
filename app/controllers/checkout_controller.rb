class CheckoutController < ApplicationController
  def create
    @cart=(JSON.parse params[:cart])
    line_items = @cart.map{ |product| a = {name: product["title"], amount: product["price"], currency: "usd", quantity: product["quantity"]}}
    @session = Stripe::Checkout::Session.create({
      customer: current_user.stripe_customer_id,
      payment_method_types: ['card'],
      line_items: line_items,
      mode: 'payment',
      success_url: checkouts_index_url,
      cancel_url: root_url,
    })
    respond_to do |format|
      format.js
    end
  end
end