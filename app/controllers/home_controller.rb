class HomeController < ApplicationController
  # before_action :confirm_role
  def index
    if current_user.has_role? :Admin
        redirect_to admins_path
    elsif current_user.has_role? :Vendor
        redirect_to vendors_path
    else
      @products=Product.all
    end
  end
end