class OrderItemsController < ApplicationController
  def show
    @order=Order.find(params[:id])
    @orderitems=@order.order_items
  end
end