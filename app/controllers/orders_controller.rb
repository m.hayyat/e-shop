class OrdersController < ApplicationController
  def index
    @orders=Order.all
  end

  def show
    user=User.find(params[:id])
    @order=user.order
  end

  def create
    @order=Order.new(order_params)
    if @order.save
      @order_item=(JSON.parse params[:cart]).map{|p| OrderItem.new(p)}
      k=@order_item.length
      k.times { |i| @order_item[i].order = @order }
      k.times { |i| @order_item[i].subtotal = params[:subtotal] }
      k.times { |i| @order_item[i].save }
      respond_to do |format|
        format.json { render :inline => "{}" }
      end
    else
      redirect_to 'checkouts/index'
    end
  end

  def edit
    @orde=Order.find(params[:id])
  end

  def update
    @orde = Order.find(params[:id])
    if @orde.update(update_params)
      flash[:notice] = "Order updated successfully."
      redirect_to orders_path
    else
      render('edit')
    end
  end

  def destroy
    order = Order.find(params[:id]).destroy
    flash[:notice] = "Order destroyed successfully."
    redirect_to order_path(:id => current_user.id)
  end

  private

  def order_params
    params.permit(:user_id, :address)
  end

  def order_item_params
    params.permit(:subtotal, cart: [] )
  end

  def update_params
    params.permit(:status)
  end
end
