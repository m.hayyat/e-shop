class ProductsController < ApplicationController
  before_action :find_user, only: [:index]
  before_action :confirm_role, only: [:new, :create, :edit, :update, :destroy]

  def index
    @products = @user.products
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:notice] = "Product created successfully."
      redirect_to products_path(:id => current_user.id)
    else
      redirect_to cart_index_path, method: :get
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update(product_params)
      flash[:notice] = "Product updated successfully."
      redirect_to products_path(:id => current_user.id)
    else
      render('edit')
    end
  end

  def delete
    @product = Product.find(params[:id])
  end

  def destroy
    product = Product.find(params[:id]).destroy
    flash[:notice] = "Product destroyed successfully."
    redirect_to products_path(:id => current_user.id)
  end

  private

    def product_params
      params.require(:product).permit(:user_id, :title, :quantity, :price, :description, images: [])
    end

    def find_user
      if params[:id]
        @user = User.find(params[:id])
      end
    end

    def confirm_role
      unless current_user.has_role? :Vendor
        redirect_to root_path, alert: "You are not authorized."
      end
  end
end
