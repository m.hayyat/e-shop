class UsersController < ApplicationController
  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
        redirect_to admins_path, notice: "User successfly updated."
    else
        render :edit
    end
  end

  private
  def user_params
    params.require(:user).permit({role_ids: []})
  end
end
