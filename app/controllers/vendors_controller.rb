class VendorsController < ApplicationController
  before_action :confirm_role
  def index
      
  end

  private
  def confirm_role
    unless current_user.has_role? :Vendor
      redirect_to root_path, alert: "You are not authorized."
    end
  end
end
