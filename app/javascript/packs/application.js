// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"

Rails.start()
Turbolinks.start()
ActiveStorage.start()

$(document).ready(function() {
    $("#placeorder").click(function() {
        const v=JSON.parse(sessionStorage.getItem('items'));
        const cartitem=JSON.stringify(v);
        const subtotal=JSON.parse(sessionStorage.getItem('subtotal'));
        const addres=JSON.parse(sessionStorage.getItem('address'));
        const uid=JSON.parse(sessionStorage.getItem('user_id'));
        $.ajax({
            url: '/orders',
            type: "POST",
            data: {
                user_id: uid,
                address: addres,
                subtotal: subtotal,
                cart: cartitem
            },
            dataType: "JSON",
            success: function(result) {
                alert('Order is placed. Thank you for shopping');
                sessionStorage.removeItem('user_id');
                sessionStorage.removeItem('items');
                sessionStorage.removeItem('subtotal');
                sessionStorage.removeItem('address');
                window.location.href='/home/index';
            },
            error: function(result) {
                alert('error');
            }
        });
    })

    $("#logout").click(function() {
        sessionStorage.removeItem('user_id');
        sessionStorage.removeItem('items');
        sessionStorage.removeItem('subtotal');
        sessionStorage.removeItem('address');
    }) 
    
    $("#buybtn").click(function() {
        const v=JSON.parse(sessionStorage.getItem('items'));
        const cartitem=JSON.stringify(v);
        $.ajax({
            url: '/checkout/create',
            type: "POST",
            data: {
                cart: cartitem
            }, 
            dataType: 'script',
            // success: function(result) {
            //     alert(result);
            // },
            // error: function(result) {
            //     alert('error');
            // }
        });
    }) 

});
  