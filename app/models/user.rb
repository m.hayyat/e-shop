class User < ApplicationRecord
  has_many :products, :dependent => :destroy
  has_many :order
  
  rolify
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable
  
  after_create :assign_default_role

  validate :must_have_a_role, on: :update

  validate :must_have_a_role, on: :new

  after_create do 
    customer = Stripe::Customer.create(email: email)
    update(stripe_customer_id: customer.id)
  end

  private

  def must_have_a_role
    unless roles.any?
      errors.add(:roles, "Must have at least 1 role")
    end
  end

  def assign_default_role
    self.add_role(:User) if self.roles.blank?
  end

end