Rails.application.routes.draw do
  get 'order_items/show'
  get 'checkouts/index'
  resources :order_items
  resources :cart
  devise_for :users, :controllers => { registrations: 'registrations'}
  resources :admins
  resources :users
  resources :products
  resources :vendors
  resources :orders
  root to: "home#index"
  get 'home/index'
  post "checkout/create", to: "checkout#create"
  post "products/add_to_cart/:id", to: "products#add_to_cart", as: "add_to_cart"
  delete "products/remove_from_cart/:id", to: "products#remove_from_cart", as: "remove_from_cart"
  # resources :webhooks, only: [:create]
end