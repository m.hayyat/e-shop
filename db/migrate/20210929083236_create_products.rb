class CreateProducts < ActiveRecord::Migration[6.1]
  def up
    create_table :products do |t|
      t.integer "user_id"
      t.string "title"
      t.integer "quantity"
      t.float "price"
      t.text "description"
      t.timestamps
    end
    add_index("products", "user_id")
  end

  def down
    drop_table :products
  end
end
