class CreateOrders < ActiveRecord::Migration[6.1]
  def up
    create_table :orders do |t|
      t.integer "user_id"
      t.string "address"
      t.string "status", :default => "Pending"
      t.timestamps
    end
    add_index("orders", "user_id")
  end

  def down
    drop_table :orders
  end
end
