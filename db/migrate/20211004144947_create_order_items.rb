class CreateOrderItems < ActiveRecord::Migration[6.1]
  def up
    create_table :order_items do |t|
      t.references :order, foreign_key: true
      t.references :product, foreign_key: true
      t.string :title
      t.integer "price"
      t.integer "quantity"
      t.integer "total"
      t.integer "subtotal"
      
    end
    # add_index :order_items, ["order_id","product_id"]
  end
  def down
    drop_table :order_items
  end
end
